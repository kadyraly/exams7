import React, { Component } from 'react';

import './App.css';
import AddItems from './components/addItems';
import OrderItems from './components/orderItems';
import RemoveItem from './components/removeItem';
import TotalPrice from './components/total';

class App extends Component {

    state = {
        items: [],
        counter: 0
    };

    componentDidMount = () => {
        const items = this.addItems();
        this.setState({items});
    }

    addItems = () => {
        const items = [
            {item: 'Hamburger', price: 80, amount: 0},
            {item: 'Cheeseburger', price: 90, amount: 0},
            {item: 'Fries', price: 45, amount: 0},
            {item: 'Coffee', price: 70, amount: 0},
            {item: 'Tea', price: 50, amount: 0},
            {item: 'Cola', price: 40, amount: 0}
        ];

        for (let i = 0; i < 6; i++) {
            const item = [];
            const itemm = {items};
            item.push(itemm);
        }

        return items;
    };


    onClick = (index) => {
        let items = [...this.state.items];
        let item = {...this.state.items[index]};
        item.amount++;
        items[index] = item;
        let counter = this.state.counter;
        counter++
        this.setState({items, counter});



    };

    removeItem = (index) => {
      this.setState({items: this.generateItems(), counter: 0})
    };



  render() {
    return (
      <div className="App">
        <AddItems buttons={this.state.items} click={this.onClick} />
        <OrderItems />
        <RemoveItem />
        <TotalPrice />
      </div>
    );
  }
}

export default App;
